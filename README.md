# 云加主站正在建设中，现在已经可以访问

[http://yunp.top](http://yunp.top)

# 云加JavaScript工程师之路

作者：梦宇

# 什么是云加工程师

[![播放](photos/play_16.png) 视频介绍云加](http://www.tudou.com/programs/view/SWmM06R4Qd0/)  

云加工程师是由梦宇提出的新概念。  

我们已经进入了云时代，所有的技术都已离不开云，在这个时代里不懂云、不会云的人将被淘汰。而云可以加一切技术，如：Android开发、iOS开发、Web前端、PHP等等，单纯的会使用这些技术的人已经不能被时代所接受了，所以时代需要云加人才，如云加Android工程师。  

梦宇致力于开发云加工程师培训课程。


# 如何观看视频教程

1. [![播放](photos/play_16.png)如何下载文件](http://www.tudou.com/programs/view/qlxQx-1ehZs/)
2. [![播放](photos/play_16.png)如何观看梦宇录制的视频教程](http://www.tudou.com/programs/view/ceU7DmZCNNY/)
3. [![播放](photos/play_16.png)《云加JavaScript工程师之路》视频教程介绍](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170405172343%E4%BA%91%E5%8A%A0JavaScript%E5%B7%A5%E7%A8%8B%E5%B8%88%E4%B9%8B%E8%B7%AF%E8%AF%BE%E7%A8%8B%E4%BB%8B%E7%BB%8D.webm)

# 推荐的计算机硬件与系统

1. 硬件要求，双核2.0或更高主频的CPU，16G或者更大的内存
2. 系统要求，Ubuntu16.04 / Windows 10 / OS X 10.11

# 第零章 开发环境准备

1. [![播放](photos/play_16.png)文本编辑器 Sublime Text](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170405195942%E6%96%87%E6%9C%AC%E7%BC%96%E8%BE%91%E5%99%A8Sublime%20Text.webm)
2. [![播放](photos/play_16.png)集成开发环境 WebStorm](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170406190622%E9%9B%86%E6%88%90%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83WebStorm%E4%BB%8B%E7%BB%8D.webm)
3. [![播放](photos/play_16.png)在 Windows 系统中使用 WebStorm](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170407154934d%E5%9C%A8Windows%E5%B9%B3%E5%8F%B0%E5%AE%89%E8%A3%85WebStorm.webm)
4. [![播放](photos/play_16.png)在 Linux 系统中使用 WebStorm](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170407160336%E5%9C%A8Linux%E5%B9%B3%E5%8F%B0%E5%AE%89%E8%A3%85WebStorm.webm)
5. [![播放](photos/play_16.png)在 OS X 系统中使用 WebStorm](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170407172712%E5%9C%A8OS%20X%E5%B9%B3%E5%8F%B0%E5%AE%89%E8%A3%85WebStorm.webm)

# 第一章 HTML

1. [![播放](photos/play_16.png)HTML发展历史](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170409105553HTML%E5%8F%91%E5%B1%95%E5%8E%86%E5%8F%B2.webm)
    * 超文本标记语言（第一版）——在1993年6月作为互联网工程工作小组（IETF）工作草案发布（并非标准）
    * HTML 2.0——1995年11月作为RFC 1866发布，在RFC 2854于2000年6月发布之后被宣布已经过时
    * HTML 3.2——1997年1月14日，W3C推荐标准
    * HTML 4.0——1997年12月18日，W3C推荐标准
    * HTML 4.01（微小改进）——1999年12月24日，W3C推荐标准
    * ISO/IEC 15445:2000（“ISO HTML”）——2000年5月15日发布，基于严格的HTML 4.01语法，是国际标准化组织和国际电工委员会的标准。
    * XHTML -- 2001 年 1 月发布的 W3C 推荐标准
    * HTML 5——2014年10月28日，W3C推荐标准
2. [![播放](photos/play_16.png)HTML文档结构](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170409122017HTML%E6%96%87%E6%A1%A3%E7%BB%93%E6%9E%84.webm)  
    
    ![html structure](photos/html_structrue.png)


2. [![播放](photos/play_16.png)元素](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170409180118%E5%85%83%E7%B4%A0.webm)。元素指的是从开始标签（start tag）到结束标签（end tag）的所有代码。
    * 顶级元素 如：html,body等
    * 块元素 如：div, p, form, ul, li, ol, dl, form, address, fieldset, hr, menu, table等
    * 内联元素（行内元素） 如：span, strong, em, br, img, input, label, select, textarea, cite等
    * 可变元素 根据上下文关系来确定元素的呈现方式，如：del

3. [![播放](photos/play_16.png)属性](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170409201845%E5%B1%9E%E6%80%A7.webm)
4. [![播放](photos/play_16.png)标题](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170409203243%E6%A0%87%E9%A2%98.webm)
    * h1
    * h2
    * h3
    * h4
    * h5
    * h6
    
6. [![播放](photos/play_16.png)样式](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170410200245%E6%A0%B7%E5%BC%8F.webm)
    * style 属性
    * 外部 css 文件
    
    应该避免使用下面这些标签和属性  
    
    | 标签 | 描述 |
    | --- | --- |
    | &lt;center&gt; | 定义居中的内容。 |
    | &lt;font&gt; 和 &lt;basefont&gt; | 定义 HTML 字体。 |
    | &lt;s&gt; 和 &lt;strike&gt; | 定义删除线文本 |
    | &lt;u&gt; | 定义下划线文本 |
    
    | 属性 | 描述 |
    | --- | --- |
    | align | 定义文本的对齐方式 |
    | bgcolor | 定义背景颜色 |
    | color | 定义文本颜色 |
    
6. [![播放](photos/play_16.png)颜色](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170411110623%E9%A2%9C%E8%89%B2.webm)
    * 英文单词
    * 16进制表示
    * rgb(Red, Green, Blue)
    * rgba(Red, Green, Blue, Alpha)
    * hsl(Hue, Saturation, Lightness)

6. [![播放](photos/play_16.png)度量单位](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170411172130%E5%BA%A6%E9%87%8F%E5%8D%95%E4%BD%8D.webm)
    * 绝对单位。如：px, in, cm, mm, deg
    * 相对单位。如：em, rem,  pt, pc, ex, ch
    * 百分比单位。如：vw, vh, vmin, vmax, %   
    
    常用单位说明表  
    
    | 单位 | 说明 |
    | --- | --- |
    | px | 像素，老旧显示器标准为每英寸72个像素，则1px是1/72英寸，如今手机屏幕每英寸一般都会超过160像素 |
    | in | 英寸 |
    | cm | 厘米 |
    | mm | 毫米 |
    | deg | 角度 |
    | em | 当前父节点所设定的字体大小 |
    | rem | 根节点所设定的字体大小 |
    | pt | 1/72 英寸 |
    | pc | 12pt |
    | ex | 取当前作用效果的字体的x的高度，在无法确定x高度的情况下以0.5em计算 |
    | ch | 以节点所使用字体中的“0”字符为基准，找不到时为0.5em |
    | vw | viewpoint width，可视区域宽度，1vw等于视窗宽度的1% |
    | vh | viewpoint height，可视区域高度，1vh等于视窗高度的1% |
    | vmin | vw和vh中较小的那个 |
    | vmax | vw和vh中较大的那个 |
    | % | 百分比 |
    
5. 段落
    * [![播放](photos/play_16.png)首行缩进](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170411175202%E6%AE%B5%E8%90%BD-%E9%A6%96%E8%A1%8C%E7%BC%A9%E8%BF%9B.webm)
    * [![播放](photos/play_16.png)段内换行](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170411182705%E6%AE%B5%E8%90%BD-%E6%AE%B5%E5%86%85%E6%8D%A2%E8%A1%8C.webm)
    
5. [![播放](photos/play_16.png)文本格式化](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170413121241%E6%96%87%E6%9C%AC%E6%A0%BC%E5%BC%8F%E5%8C%96.webm)

    | 标签 | 说明 |
    | --- | --- |
    | b | 粗体 |
    | strong | 着重，相比粗体来说对搜索引擎更加友好 |
    | big | 大文字 |
    | em | 强调 |
    | i | 斜体 |
    | small | 小文字 |
    | sub | subscript(下标) |
    | sup | superscript(上标) |
5. 预格式文本
    * [![播放](photos/play_16.png)pre](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170413122521pre.webm)
    * [![播放](photos/play_16.png)code](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170413123022code.webm)
    * [![播放](photos/play_16.png)计算机代码高亮](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170413123611%E8%AE%A1%E7%AE%97%E6%9C%BA%E4%BB%A3%E7%A0%81%E9%AB%98%E4%BA%AE.webm)
5. [![播放](photos/play_16.png)地址](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170415095537%E5%9C%B0%E5%9D%80.webm)
5. [![播放](photos/play_16.png)缩写](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170415095915%E7%BC%A9%E5%86%99.webm)
5. [![播放](photos/play_16.png)文字方向](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170415102546%E6%96%87%E5%AD%97%E6%96%B9%E5%90%91.webm)
5. [![播放](photos/play_16.png)引用](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170415103731%E5%BC%95%E7%94%A8.webm)
    * 引用
    * 块引用
5. [![播放](photos/play_16.png)删除字效果和插入字效果](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170415104514%E5%88%A0%E9%99%A4%E6%96%87%E5%AD%97%E5%92%8C%E6%8F%92%E5%85%A5%E6%96%87%E5%AD%97%E7%9A%84%E6%95%88%E6%9E%9C.webm)
    
10. [![播放](photos/play_16.png)注释](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170415120158%E6%B3%A8%E9%87%8A.webm)
    * 注释
    * IE条件注释
    
    IE条件注释  
    
    | 关键字 | 意思 |
    | --- | --- |
    | if IE 7 | 代表如果是IE7的话解析该注释内容 |
    | if !IE 7 | 代表如果不是IE7的话解析该注释内容 |
    | if gt IE7 | 代表如果大于IE7的话解析该注释内容 |
    | if gte | 代表如果大于或者等于IE7的话解析该注释内容 |
    | if lt IE7 | 代表如果小于IE7的话解析该注释内容 |
    | if lte IE7 | 代表如果小于或等于IE7的话解析该注释内容 |  
    
11. [![播放](photos/play_16.png)超链接](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170415123035%E8%B6%85%E9%93%BE%E6%8E%A5.webm)
    * 地址链接
    * 邮箱链接
    
12. 图像与图形  
    * [![播放](photos/play_16.png)位图jpg与png](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170417192744%E4%BD%8D%E5%9B%BEjpg%E5%92%8Cpng.webm)
    * [![播放](photos/play_16.png)动图gif](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170418082450gif%E5%8A%A8%E5%9B%BE.webm)
    * [![播放](photos/play_16.png)矢量图svg](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170418083835%E7%9F%A2%E9%87%8F%E5%9B%BEsvg.webm)
    
13. 表格
    * [![播放](photos/play_16.png)表格的基本用法](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420153236%E8%A1%A8%E6%A0%BC%E7%9A%84%E5%9F%BA%E6%9C%AC%E7%94%A8%E6%B3%95.webm)
    * [![播放](photos/play_16.png)合并单元格](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420153801%E5%90%88%E5%B9%B6%E5%8D%95%E5%85%83%E6%A0%BC.webm)
    * [![播放](photos/play_16.png)表格分断](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420154812%E8%A1%A8%E6%A0%BC%E5%88%86%E6%96%AD.webm)
    * [![播放](photos/play_16.png)tbody滚动](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420155538tbody%E6%BB%9A%E5%8A%A8.webm)
14. 列表
    * [![播放](photos/play_16.png)有序列表](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420190855%E6%9C%89%E5%BA%8F%E5%88%97%E8%A1%A8.webm)
    * [![播放](photos/play_16.png)无序列表](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420191113%E6%97%A0%E5%BA%8F%E5%88%97%E8%A1%A8.webm)
    
15. [![播放](photos/play_16.png)框架](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420191353%E6%A1%86%E6%9E%B6iframe.webm)
17. [![播放](photos/play_16.png)实体](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420192534%E5%AE%9E%E4%BD%93.webm)
    
    **常用实体**  
    
    | 结果 | 描述 | 实体名称 | 实体编号 |
    | --- | --- | --- | --- |
    | &quot; | quotation mark | \&quot; | \&#34; |
    | &apos; | apostrophe | \&apos; | \&#39; |
    | & | ampersand | \&amp; | \&#38; |
    | &lt; | less-than | \&lt; | \&#60; |
    | &gt; | greater-than | \&gt; | \&#62; |
    
    [HTML实体参考表](docs/HtmlSymbols.md)
    
20. [![播放](photos/play_16.png)腾讯云服务器搭建静态网站](http://git.oschina.net/yun-p/yun-p-js/raw/master/videos/20170420193432%E4%BD%BF%E7%94%A8%E8%85%BE%E8%AE%AF%E4%BA%91%E6%9C%8D%E5%8A%A1%E5%99%A8%E6%90%AD%E5%BB%BA%E9%9D%99%E6%80%81%E7%BD%91%E7%AB%99.mp4)


# 第二章 CSS

1. CSS语法
1. 背景
1. 边框
1. 文本
1. 字体
1. 链接
1. 列表
1. 表格
1. 轮廓
2. 选择器
4. 盒子模型
5. 弹性盒子模型
6. 定位
7. 对齐
8. 尺寸
9. 媒体
11. CSS变换
    * 2D变换
    * 3D变换
11. 动画
12. 过渡
13. 提升CSS开发效率


# 第三章 JavaScript

1. JavaScript语言发展历史
2. 变量与常量
2. 基本数据类型
3. 函数
3. 函数闭包
4. 流程控制
5. 循环
6. 字符串
6. 正则表达式
7. 数组
8. 对象
9. Map

# 第四章 面向对象

1. 命名空间
2. 面向对象的理解与实现
3. prototype
4. 类

# 第五章 事件机制

1. 事件侦听器
2. 事件的行为
3. 可取消事件
4. 派发事件
5. 派发可取消的事件

# 第六章 工程化

1. 依赖项管理  
    * npm
    * cnpm
    * bower
    
2. 提升JavaScript开发效率  
    * ES6编译工具
    * TypeScript
    
2. 构建工具  
    * grunt
    * gulp
    
3. 打包工具Webpack
4. 生成器

# 第七章 用户交互

1. 鼠标交互
2. 触控交互
3. 拖拽

# 第八章 多媒体

1. 播放声音
2. 播放视频
3. 拍照
4. 录音
5. 录像
6. WebRTC

# 第九章 绘图

1. 2D绘图
2. 3D绘图

# 第十章 常用框架

1. jQuery
    * 选择器
    * 动画
    * DOM操作
    * 样式控制
    * ajax
    
2. jQuery UI
3. jQuery mobile
4. Bootstrap
5. AngularJS  
    * AngularJS 1
    * AngularJS 2
5. Vue.js
6. React

# 第十一章 移动应用开发
1. 开发Android应用
2. 开发iOS应用
3. 开发微信小程序

# 第十二章 桌面软件开发
1. electron与nwjs
2. 使用electron开发桌面软件
3. 主进程(Main Process)与渲染进程(Renderer Process)
3. 菜单
4. 通知
5. 应用图标
6. 系统托盘图标
7. 无框窗体
8. 文件操作
9. 剪贴板

# 第十三章 Node.js

1. Express
1. MySQL
2. MongoDB
3. ORM
    * Sequelize
    * orm2
    * q-orm

# 参考资料

* [http://baike.baidu.com/item/HTML](http://baike.baidu.com/item/HTML)
* [http://www.w3school.com.cn/html/](http://www.w3school.com.cn/html/)

# 捐助

如果您觉得我的工作对您有所帮助，请支持我继续，谢谢。   

![捐助](photos/donate.jpg)